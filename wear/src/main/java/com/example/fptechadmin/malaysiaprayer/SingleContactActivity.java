package com.example.fptechadmin.malaysiaprayer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by FPtechadmin on 11/6/15.
 */
public class SingleContactActivity extends Activity {

    // JSON node keys
    private static final String TAG_TEMPAT = "tempat";
    private static final String TAG_HDATE = "hdate";
    private static final String TAG_KOD = "kod";
    private static final String TAG_IMSAK = "imsak";
    private static final String TAG_SUBUH = "subuh";
    private static final String TAG_SYURUK = "syuruk";
    private static final String TAG_ZOHOR = "zohor";
    private static final String TAG_ASAR = "asar";
    private static final String TAG_MAGHRIB = "maghrib";
    private static final String TAG_ISYAK = "isyak";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_contact);

        // getting intent data
        Intent in = getIntent();

        // Get JSON values from previous intent
        String tempat = in.getStringExtra(TAG_TEMPAT);
        String hdate = in.getStringExtra(TAG_HDATE);
        String kod = in.getStringExtra(TAG_KOD);
        String imsak = in.getStringExtra(TAG_IMSAK);
        String subuh = in.getStringExtra(TAG_SUBUH);

        // Phone node is JSON Object
        //JSONObject phone = c.getJSONObject(TAG_PHONE);
        String syuruk = in.getStringExtra(TAG_SYURUK);
        String zohor = in.getStringExtra(TAG_ZOHOR);
        String asar = in.getStringExtra(TAG_ASAR);
        String maghrib = in.getStringExtra(TAG_MAGHRIB);
        String isyak = in.getStringExtra(TAG_ISYAK);

        // Displaying all values on the screen
        TextView lbltempat = (TextView) findViewById(R.id.tempat_label);
        TextView lblhdate = (TextView) findViewById(R.id.hdate_label);
        TextView lblkod = (TextView) findViewById(R.id.kod_label);
        TextView lblimsak = (TextView) findViewById(R.id.imsak_label);
        TextView lblsubuh = (TextView) findViewById(R.id.subuh_label);
        TextView lblsyuruk = (TextView) findViewById(R.id.syuruk_label);
        TextView lblzohor = (TextView) findViewById(R.id.zohor_label);
        TextView lblasar = (TextView) findViewById(R.id.asar_label);
        TextView lblmaghrib = (TextView) findViewById(R.id.maghrib_label);
        TextView lblisyak = (TextView) findViewById(R.id.isyak_label);

        lbltempat.setText(tempat);
        lblhdate.setText(hdate);
        lblkod.setText(kod);
        lblimsak.setText(imsak);
        lblsubuh.setText(subuh);
        lblsyuruk.setText(syuruk);
        lblzohor.setText(zohor);
        lblasar.setText(asar);
        lblmaghrib.setText(maghrib);
        lblisyak.setText(isyak);
    }
}
