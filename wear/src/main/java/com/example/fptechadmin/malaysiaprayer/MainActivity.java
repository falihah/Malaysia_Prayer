package com.example.fptechadmin.malaysiaprayer;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends ListActivity {

    private String[] listItems;
    private WearableListView mWearableListView;
    private ProgressDialog pDialog;

    // URL to get contacts JSON
    //private static String url = "http://api.androidhive.info/contacts/";
    private static String url = "http://eroy.me-tech.com.my/api.jakim.php?kod=SGR01";

    // JSON Node names
    private static final String TAG_TEMPAT = "tempat";
    private static final String TAG_HDATE = "hdate";
    private static final String TAG_KOD = "kod";
    private static final String TAG_IMSAK = "imsak";
    private static final String TAG_SUBUH = "subuh";
    private static final String TAG_SYURUK = "syuruk";
    private static final String TAG_ZOHOR = "zohor";
    private static final String TAG_ASAR = "asar";
    private static final String TAG_MAGHRIB = "maghrib";
    private static final String TAG_ISYAK = "isyak";

    // contacts JSONArray
    JSONArray contacts = null;

    // Hashmap for ListView
    ArrayList<HashMap<String, String>> contactList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*listItems = new String[]{"item0", "item1", "item2", "item3", "item4"};
        mWearableListView = (WearableListView) findViewById(R.id.list);
        mWearableListView.setAdapter(new SimpleListAdapter(this, listItems));
        mWearableListView.setClickListener(new WearableListView.ClickListener() {
            @Override
            public void onClick(WearableListView.ViewHolder viewHolder) {
                int position = viewHolder.getPosition();
                Toast.makeText(MainActivity.this, "Tapped on item " + position,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTopEmptyRegionClick() {
                Toast.makeText(MainActivity.this, "Tapped on top empty region",
                        Toast.LENGTH_LONG).show();
            }
        });*/

        contactList = new ArrayList<HashMap<String, String>>();

        ListView lv = getListView();

        // Listview on item click listener
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String tempat = ((TextView) view.findViewById(R.id.tempat))
                        .getText().toString();
                String hdate = ((TextView) view.findViewById(R.id.hdate))
                        .getText().toString();
                String kod = ((TextView) view.findViewById(R.id.kod))
                        .getText().toString();
                String imsak = ((TextView) view.findViewById(R.id.imsak))
                        .getText().toString();
                String subuh = ((TextView) view.findViewById(R.id.subuh))
                        .getText().toString();
                String syuruk = ((TextView) view.findViewById(R.id.syuruk))
                        .getText().toString();
                String zohor = ((TextView) view.findViewById(R.id.zohor))
                        .getText().toString();
                String asar = ((TextView) view.findViewById(R.id.asar))
                        .getText().toString();
                String maghrib = ((TextView) view.findViewById(R.id.maghrib))
                        .getText().toString();
                String isyak = ((TextView) view.findViewById(R.id.isyak))
                        .getText().toString();


                // Starting single contact activity
                Intent in = new Intent(getApplicationContext(),
                        SingleContactActivity.class);
                in.putExtra(TAG_TEMPAT, tempat);
                in.putExtra(TAG_HDATE, hdate);
                in.putExtra(TAG_KOD, kod);
                in.putExtra(TAG_IMSAK, imsak);
                in.putExtra(TAG_SUBUH, subuh);
                in.putExtra(TAG_SYURUK, syuruk);
                in.putExtra(TAG_ZOHOR, zohor);
                in.putExtra(TAG_ASAR, asar);
                in.putExtra(TAG_MAGHRIB, maghrib);
                in.putExtra(TAG_ISYAK, isyak);
                startActivity(in);

            }
        });

        // Calling async task to get json
        new GetContacts().execute();
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject e = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    //contacts = jsonObj.getJSONArray(TAG_TEMPAT);

                    // looping through All Contacts
                    for (int i = 0; i < e.length(); i++) {
                        //JSONObject e = contacts.getJSONObject(i);

                        String tempat = e.getString(TAG_TEMPAT);
                        String hdate = e.getString(TAG_HDATE);
                        String kod = e.getString(TAG_KOD);
                        String imsak = e.getString(TAG_IMSAK);
                        String subuh = e.getString(TAG_SUBUH);

                        // Phone node is JSON Object
                        //JSONObject phone = c.getJSONObject(TAG_PHONE);
                        String syuruk = e.getString(TAG_SYURUK);
                        String zohor = e.getString(TAG_ZOHOR);
                        String asar = e.getString(TAG_ASAR);
                        String maghrib = e.getString(TAG_MAGHRIB);
                        String isyak = e.getString(TAG_ISYAK);

                        // tmp hashmap for single contact
                        HashMap<String, String> contact = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        contact.put(TAG_TEMPAT, tempat);
                        contact.put(TAG_HDATE, hdate);
                        contact.put(TAG_KOD, kod);
                        contact.put(TAG_IMSAK, imsak);
                        contact.put(TAG_SUBUH, subuh);
                        contact.put(TAG_SYURUK, syuruk);
                        contact.put(TAG_ZOHOR, zohor);
                        contact.put(TAG_ASAR, asar);
                        contact.put(TAG_MAGHRIB, maghrib);
                        contact.put(TAG_ISYAK, isyak);


                        // adding contact to contact list
                        contactList.add(contact);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    MainActivity.this, contactList,
                    R.layout.list_item, new String[] { TAG_TEMPAT, TAG_HDATE,
                    TAG_KOD,TAG_IMSAK,TAG_SUBUH,TAG_SYURUK,TAG_ZOHOR,TAG_ASAR,TAG_MAGHRIB,TAG_ISYAK }, new int[] { R.id.tempat,
                    R.id.hdate, R.id.kod,R.id.imsak,R.id.subuh,R.id.syuruk,R.id.zohor,R.id.asar,R.id.maghrib,R.id.isyak });

            setListAdapter(adapter);
        }

    }


}