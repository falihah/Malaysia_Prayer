package com.example.fptechadmin.malaysiaprayer;

/**
 * Created by FPtechadmin on 11/6/15.
 */
import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * The WearableListView.Adapterclass is responsible for instantiating the views
 * used for every row in a WearableListViewand for binding a row to the appropriate
 * data.
 */
public class SimpleListAdapter extends WearableListView.Adapter {
    private LayoutInflater mInflater;
    private String[] mSampleItems;

    /**
     * Constructor
     *
     * @param context
     * @param sampleItems
     */
    public SimpleListAdapter(Context context, String[] sampleItems) {
        mInflater = LayoutInflater.from(context);
        mSampleItems = sampleItems;
    }

    /**
     * create a new view for a row in the list
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = mInflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        return new WearableListView.ViewHolder(view);
    }

    /**
     * To determines the position (that is index) of every visible
     * row in the list
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(WearableListView.ViewHolder viewHolder, int position) {
        TextView textView = (TextView) viewHolder.itemView;
        textView.setText(mSampleItems[position]);
    }

    /**
     * To know how many items are in the list
     * @return
     */
    @Override
    public int getItemCount() {
        return mSampleItems.length;
    }

}
