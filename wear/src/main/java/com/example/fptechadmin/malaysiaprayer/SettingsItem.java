package com.example.fptechadmin.malaysiaprayer;

/**
 * Created by FPtechadmin on 11/5/15.
 */
public class SettingsItem {

    public SettingsItem(int iconRes, String title) {
        this.iconRes = iconRes;
        this.title = title;
    }
    public int iconRes;
    public String title;

}
