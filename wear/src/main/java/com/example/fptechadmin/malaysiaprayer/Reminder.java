package com.example.fptechadmin.malaysiaprayer;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Reminder extends Activity implements WearableListView.ClickListener {

    private WearableListView listView;
    private String[] mItems = {"5 minute", "10 minute","15 minute","20 minute","30 minute","40 minutes","50 minutes","60 minutes"};
    private int[] mImages = {R.drawable.ic_cc_alarm, R.drawable.ic_cc_alarm,R.drawable.ic_cc_alarm,R.drawable.ic_cc_alarm, R.drawable.ic_cc_alarm,R.drawable.ic_cc_alarm, R.drawable.ic_cc_alarm,R.drawable.ic_cc_alarm};
    private TextView mHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        // This is our list header
       // mHeader = (TextView) findViewById(R.id.header);
        listView = (WearableListView) findViewById(R.id.list);
        loadAdapter();
    }

    private void loadAdapter() {
        List<SettingsItem> items = new ArrayList<>();
        for (int i=0; i<mItems.length; i++){
            items.add(new SettingsItem(mImages[i], mItems[i]));
        }

        CustomListAdapter mAdapter = new CustomListAdapter(this, items);


        listView.setAdapter(mAdapter);
        listView.addOnScrollListener(mOnScrollListener);
        listView.setClickListener(this);
    }


    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        switch (viewHolder.getPosition()) {
            case 0:
                //Do something
                break;
            case 1:
                //Do something else
                break;
        }
    }

    @Override
    public void onTopEmptyRegionClick() {
        //Prevent NullPointerException
    }

    // The following code ensures that the title scrolls as the user scrolls up
    // or down the list
    private WearableListView.OnScrollListener mOnScrollListener =
            new WearableListView.OnScrollListener() {
                @Override
                public void onAbsoluteScrollChange(int i) {
                    // Only scroll the title up from its original base position
                    // and not down.
                    if (i > 0) {
                        mHeader.setY(-i);
                    }
                }

                @Override
                public void onScroll(int i) {
                    // Placeholder
                }

                @Override
                public void onScrollStateChanged(int i) {
                    // Placeholder
                }

                @Override
                public void onCentralPositionChanged(int i) {
                    // Placeholder
                }
            };
}